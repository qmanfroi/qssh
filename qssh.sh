#!/bin/bash

set -e
set -o pipefail

host=
#grep -E '^Host.*bastion' config 
match=$(grep -iE "^Host.*(${1,,}.*$2|$2.*${1,,}).*bastion" $HOME/.ssh/config | cut -d' ' -f2)
if [[ $(echo "$match" | wc -l) -le 1 ]]; then
    if [ -z "$match" ]; then
        echo "No match with ssh config, exiting .."
        exit 1
    fi
    host=$match
else
    SAVEIFS=$IFS
    IFS=$(echo -en "\n\b")
    c=0
    for i in $match; do 
        c=$((c+1))
        echo "$c.$i"
    done
    read -p "Make a choice (1-$c)" choice
    IFS=$SAVEIFS
    host=$(echo $match | cut -d' ' -f $choice)
fi

ssh-add -D > /dev/null 2>&1

if ls $HOME/.ssh/keys/*${1,,}*/* > /dev/null 2>&1; then
    ssh-add $HOME/.ssh/keys/*${1,,}*/*
elif [[ $(grep -A 10 -iE "^Host.*(${1,,}.*$2|$2.*${1,,}).*bastion" $HOME/.ssh/config | grep IdentityFile | head -n 1) =~ ^.*\.ssh/keys/([[:alnum:]]*)/.*$  ]] ; then 
    other_match=${BASH_REMATCH[1],,}
    if ls $HOME/.ssh/keys/*${other_match}*/* > /dev/null 2>&1; then
        ssh-add $HOME/.ssh/keys/*${other_match}*/*
    fi
fi
echo "Connection to : $host"
ssh $host
